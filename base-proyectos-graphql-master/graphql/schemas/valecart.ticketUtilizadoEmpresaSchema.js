'use strict';
const types = `
	type giftCardData{
		id : Int
		empresa : String
		giftcard : String
		orden : Int
		activo : Int
		tipoComercio : Int
		imprimible : Int
		codificacion : String
		x : String
		y : String
		clave : Int
		codigoTarjeta : Int
		condicionesHTML : String
		condicionesPlana : String
		imagenGiftCardPNG : String
		imagenGiftCardJPG : String
		fechaCreacion: String
		imgPortal : String
		imgBanner : String
		color : String
		colorFooterEmail : String
		emailLogoBlanco : Int
		template : String
		descripcion : String
		enCertificado : Int
		idCategoria: Int
		stockBajo: Int
		stockCritico: Int
		tags: [tagDataGC]
	}
	type tagDataGC{
		id : Int
		tag  : String
	}
	
	type respuestaGC{
		status: String
		message: String
		data: giftCardData
	}
`;

const inputs = `
`;

const queries = `
	getGiftCard(gc:ID!): respuestaGC
`;

const mutations = `
`;

module.exports = {types:types, inputs:inputs, queries:queries, mutations:mutations};