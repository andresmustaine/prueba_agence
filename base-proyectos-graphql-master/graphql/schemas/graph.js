'use strict';
const { gql } = require('apollo-server-express');
const u = require('../../controllers/utilidadesController');
let files = u.getFilesNamesInFolderFilter('./graphql/schemas','Schema.js');

var schemas=[];
files.forEach((f)=>{
	schemas.push(require('./'+f));
});

var types = [];
var queries = [];
var mutations = [];
var inputs = [];

schemas.forEach(s =>{
	types.push(s.types);
	queries.push(s.queries);
	mutations.push(s.mutations);
	inputs.push(s.inputs);
});

let schemaGraphql =gql`
	${types.join('\n')}
	${inputs.join('\n')}
	type Query {
		${queries.join('\n')}
	}
	type Mutation {
		${mutations.join('\n')}
	}
`;

module.exports = schemaGraphql;