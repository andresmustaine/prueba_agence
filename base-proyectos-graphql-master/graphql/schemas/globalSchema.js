'use strict';
const types = `
	type respuestaSinData{
		status: String
		message: String
	}
`;
const inputs = `
	input inputTabla{
		draw: Int
		search: String
		start: Int
		len: Int
		orderCol: String
		orderType: String
	}
`;

const queries = `
	sisInfo: respuestaSinData!
`;

const mutations = `
	walletInfo: respuestaSinData!
`;

module.exports = {types:types, inputs:inputs, queries:queries, mutations:mutations};