"use strict";

const mongoMessengerCon = new function(){
	const mongo = require('../../controllers/mongooseController');
	let con = mongo.conexion(process.env.mongoCon);
	this.auth = require('./mongodb/messenger.auth')(con,"auth");

	return this;
}

const mongoMessengerExport = {fast: mongoMessengerCon,slow: mongoMessengerCon,complex: mongoMessengerCon};

module.exports = mongoMessengerExport;