'use strict';

module.exports = (db, dt) =>{
	return db.con.define('ticketTags',
	{
		id :{
			type: dt.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tag :{
			type: dt.STRING,
			allowNull : false
		},
		estado :{
			type: dt.INTEGER,
			allowNull : true
		},
		fechaCreacion: { type: dt.STRING }
	},
	{
		createdAt: false,
		updatedAt: false,
		deletedAt: false,
		freezeTableName: true,
		underscored: true
	});
}
