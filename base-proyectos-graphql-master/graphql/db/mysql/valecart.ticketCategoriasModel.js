'use strict';

module.exports = (db, dt) =>{
	return db.con.define('ticketCategorias',
	{
		id :{
			type: dt.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		categoria :{
			type: dt.STRING,
			allowNull : false
		},
		icono :{
			type: dt.STRING,
			allowNull : false
		},
		activo :{
			type: dt.INTEGER,
			allowNull : false
		},
		visible :{
			type: dt.INTEGER,
			allowNull : false
		},
		orden :{
			type: dt.INTEGER,
			allowNull : false
		},
		fechaCreacion: { type: dt.STRING }
	},
	{
		createdAt: false,
		updatedAt: false,
		deletedAt: false,
		freezeTableName: true,
		underscored: true
	});
}