'use strict';

module.exports = (db, dt) =>{
	return db.con.define('ticketUtilizadoEmpresaTags',
	{
		id :{
			type: dt.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		idTag :{
			type: dt.INTEGER,
			allowNull : false
		},
		idGiftcard :{
			type: dt.INTEGER,
			allowNull : false
		},
		alias: {
			type: dt.STRING,
			allowNull : false
		},
		estado :{
			type: dt.INTEGER,
			allowNull : true
		}
	},
	{
		createdAt: false,
		updatedAt: false,
		deletedAt: false,
		freezeTableName: true,
		underscored: true
	});
}