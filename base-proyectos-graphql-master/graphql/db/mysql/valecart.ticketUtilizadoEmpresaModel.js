'use strict';

module.exports = (db, dt) =>{
	return db.con.define('ticketUtilizadoEmpresa',
	{
		id :{
			type: dt.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		empresa :{
			type: dt.STRING,
			allowNull : false
		},
		giftcard :{
			type: dt.STRING,
			allowNull : false
		},
		orden :{
			type: dt.INTEGER,
			allowNull : true
		},
		activo :{
			type: dt.INTEGER,
			allowNull : false
		},
		tipoComercio :{
			type: dt.INTEGER,
			allowNull : false
		},
		imprimible :{
			type: dt.INTEGER,
			allowNull : false
		},
		codificacion :{
			type: dt.STRING,
			allowNull : false
		},
		x :{
			type: dt.FLOAT,
			allowNull : false
		},
		y :{
			type: dt.FLOAT,
			allowNull : false
		},
		clave :{
			type: dt.INTEGER,
			allowNull : false
		},
		codigoTarjeta :{
			type: dt.INTEGER,
			allowNull : false
		},
		codificacion :{
			type: dt.STRING,
			allowNull : false
		},
		condicionesHTML :{
			type: dt.TEXT,
			allowNull : false
		},
		condicionesPlana :{
			type: dt.TEXT,
			allowNull : false
		},
		imagenGiftCardPNG :{
			type: dt.TEXT,
			allowNull : true
		},
		imagenGiftCardJPG :{
			type: dt.TEXT,
			allowNull : true
		},
		fechaCreacion: { type: dt.STRING },
		imgPortal :{
			type: dt.TEXT,
			allowNull : true
		},
		imgBanner :{
			type: dt.TEXT,
			allowNull : true
		},
		color :{
			type: dt.STRING,
			allowNull : false
		},
		colorFooterEmail :{
			type: dt.STRING,
			allowNull : false
		},
		emailLogoBlanco :{
			type: dt.INTEGER,
			allowNull : false
		},
		template :{
			type: dt.STRING,
			allowNull : false
		},
		descripcion :{
			type: dt.TEXT,
			allowNull : false
		},
		enCertificado:{
			type: dt.INTEGER,
			allowNull : true
		},
		idCategoria:{
			type: dt.INTEGER,
			allowNull : false
		},
		tsUpdImg:{
			type: dt.STRING,
			allowNull : true
		},
		stockBajo :{
			type: dt.INTEGER,
			allowNull : true,
			defaultValue: 10
		},
		stockCritico :{
			type: dt.INTEGER,
			allowNull : true,
			defaultValue: 5
		}
	},
	{
		createdAt: false,
		updatedAt: false,
		deletedAt: false,
		freezeTableName: true,
		underscored: true,
		hooks: {
			afterCreate: function(ticketUtilizadoEmpresa, options, cb) {
				let gcUpdate = {
			            orden: ticketUtilizadoEmpresa.id
			            ,imagenGiftCardPNG: ticketUtilizadoEmpresa.imagenGiftCardPNG+'/'+ticketUtilizadoEmpresa.id+'.png'
						,imagenGiftCardJPG: ticketUtilizadoEmpresa.imagenGiftCardJPG+'/'+ticketUtilizadoEmpresa.id+'.jpg'
			        };
				if(ticketUtilizadoEmpresa.tipoComercio === 1)
				{
					gcUpdate.imgPortal= ticketUtilizadoEmpresa.imgPortal+'/'+ticketUtilizadoEmpresa.id+'_'+ticketUtilizadoEmpresa.giftcard+'_portal.jpg'
					gcUpdate.imgBanner= ticketUtilizadoEmpresa.imgBanner+'/'+ticketUtilizadoEmpresa.id+'_'+ticketUtilizadoEmpresa.giftcard+'_banner.jpg';
				}
			   return ticketUtilizadoEmpresa.update(gcUpdate) .catch(err => {console.log(err)});
			}
		}
	});
}