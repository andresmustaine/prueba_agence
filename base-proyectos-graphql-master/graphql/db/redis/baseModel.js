'use strict';
module.exports = (db,key) => {
	let promiseFunct = function(err, response,resolve,reject){
		if (err) {
            reject(err);
        } else {
            resolve(response);
        }
	};

	this.find = async function(_id)
	{
	    return new Promise(function(resolve, reject) {
	        db.get(key+":"+_id, function(err, response){
	         promiseFunct(err, response,resolve,reject);
	    	});
	    });
	};

	this.findAll = async function()
	{
	    return new Promise(function(resolve, reject) {
	        db.keys(key+'*', function(err, response){
	        	if(response.length > 0){
		        	db.mget(response, function(err, response2){
		         		promiseFunct(err, response2,resolve,reject);
		        	});
		        }
		        else
		        {
		        	promiseFunct([], ['No genero datos'],resolve,reject);
		        }
	    	});
	    });
	};

	this.set = async function(_id,_data)
	{
	    return new Promise(function(resolve, reject) {
	        db.set(key+":"+_id,_data,function(err, response){
	         promiseFunct(err, response,resolve,reject);
	    	});
	    });
	};

	this.clear = async function(){
		return new Promise(function(resolve, reject) {
	        db.DEL(key,function(err, response){
	         promiseFunct(err, response,resolve,reject);
	    	});
	    });
	}

	return this;
};