'use strict';

const redis = new function(){
	const redisCon = require('../../controllers/redisController');
	let con = redisCon.conexion(process.env.redisConHost,process.env.redisSelectDB,process.env.redisPort);
	
}

const redisExport = {fast:redis, slow:redis, complex:redis};
module.exports = redisExport;