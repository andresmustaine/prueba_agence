'use strict';

const dbValecart = new function(){
	let mysqlCon = require('../../controllers/mysqlController');
	let con = mysqlCon.conexion(process.env.mysqlConHost,process.env.mysqlConDB2, process.env.mysqlConUser, process.env.mysqlConPass);
	this.sq = con.sq;
	this.ticketUtilizadoEmpresa = require('./mysql/valecart.ticketUtilizadoEmpresaModel')(con, con.sq);
	this.ticketCategoria = require('./mysql/valecart.ticketCategoriasModel')(con, con.sq);
	this.ticketTags = require('./mysql/valecart.ticketTagsModel')(con, con.sq);
	this.ticketUtilizadoEmpresaTags = require('./mysql/valecart.ticketUtilizadoEmpresaTagsModel')(con, con.sq);


	/**
	 * De momento usar hasMany para las relaciones
	 * hasOne ----> sourceKey
	 * belongsTo --->use targetKey to set the foreigner key from
	 * hasMany --->use sourceKey to set the foreigner key from
	 */

	return this;
}
const dbValecartExport = {fast: dbValecart, slow: dbValecart, complex:dbValecart};
module.exports = dbValecartExport;