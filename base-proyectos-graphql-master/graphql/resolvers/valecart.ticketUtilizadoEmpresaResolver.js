'use strict';

const dataMap = require('../../controllers/dataMapController');

const QueriesResolvers = {
	getGiftCard: async (parent,args,{dbValecart}) =>{
		let gcs = await dbValecart.fast.ticketUtilizadoEmpresa.find({where:{id:args.gc}});
		return dataMap.generarRespuesta(gcs);
	},
};
const MutationsResolvers = {
};
module.exports = { QueriesResolvers: QueriesResolvers, MutationsResolvers:MutationsResolvers };