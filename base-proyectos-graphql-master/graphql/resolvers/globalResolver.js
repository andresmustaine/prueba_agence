'use strict';

const dataMap = require('../../controllers/dataMapController');

const QueriesResolvers = {
	sisInfo: async (parent,args,{ }) =>{
		return {'status':'success', 'message': 'Servicio dedicado a '+process.env.PROYECTO+''};
	},
};
const MutationsResolvers = {
	walletInfo: async (parent,args,{ }) =>{
		return {'status':'success', 'message': process.env.PROYECTO+' version: '+process.env.VERSION+''};
	},
};
module.exports = { QueriesResolvers: QueriesResolvers, MutationsResolvers:MutationsResolvers };