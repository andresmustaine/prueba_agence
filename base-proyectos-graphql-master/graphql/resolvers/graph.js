'use strict';
const u = require('../../controllers/utilidadesController');
let files = u.getFilesNamesInFolderFilter('./graphql/resolvers','Resolver.js');
var resolvers=[];
files.forEach((f)=>{
	resolvers.push(require('./'+f));
});

const QueryResolvers = {};
const MutationResolvers = {};

resolvers.forEach(r =>{
 for (let key in r) {
	if(key==='QueriesResolvers'){
		for(let subKeyQuery in r[key]){
			QueryResolvers[subKeyQuery] = r[key][subKeyQuery];
		}
	}
	if(key==='MutationsResolvers'){
		for(let subKeyMutation in r[key]){
			MutationResolvers[subKeyMutation] = r[key][subKeyMutation];
		}
	}
 }
});

module.exports = {Query: QueryResolvers , Mutation: MutationResolvers};