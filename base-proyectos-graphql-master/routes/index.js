var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { powered: 'Powered By Dcanje.com', version: process.env.VERSION, proyecto: process.env.PROYECTO});
});

module.exports = router;
