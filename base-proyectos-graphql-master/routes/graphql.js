"use strict";
const { ApolloServer } = require('apollo-server-express');
const constraintDirective = require('graphql-constraint-directive');

var schemas = require('../graphql/schemas/graph');

var resolvers = require('../graphql/resolvers/graph');

const server = new ApolloServer({
	typeDefs: schemas, 
	resolvers: resolvers,
	introspection: true,
  	playground: process.env.graphQlPlayGround,
  	schemaDirectives: { constraint: constraintDirective },
	context: ({req, res}) => (
	    require('../graphql/graphModels')(req, res)
	)
});

module.exports = server;