'use strict';
const dateFormat = require('dateformat'),
fs = require('fs');

const Utilidades = new function() {
	this.getParseDateFromString = function(_stringDate,_format)
	{
		let newDate = new Date(_stringDate);
		return dateFormat(newDate,_format);
	};

	this.getFileNamesInFolder = function(_folder){
		return fs.readdirSync(_folder);
	};

	this.getFilesNamesInFolderFilter = function(_folder,_filter){
		let filesArray = this.getFileNamesInFolder(_folder);
		let filesRet = [];
		filesArray.forEach((f)=>{
			if(new RegExp('[a-zA-Z0-9]'+_filter).test(f)){
				filesRet.push(f);
			}
		});
		return filesRet;
	};
	this.readFile= function(_file){ console.log(_file)
		return fs.readFile(_file, 'utf8',(e,data)=>{
			if(e){
				return console.log(e);
			}
			console.log(data);
		});
	}
};

module.exports = Utilidades;