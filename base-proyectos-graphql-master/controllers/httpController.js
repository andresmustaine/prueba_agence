'use strict';
const http = require("http"),
	https = require("https"),
	request = require('request');

const HttpRequest = new function() {
	this.get = function(_url,_callback,_secure = true)
	{
		let requestSender = (_secure === true)? https : http;
		let req = requestSender.get(
			{
				host:_url.host
				,path:_url.path
				,rejectUnauthorized: false
			}, (res) => {
				let dataRetorno ="";
				res.on('data', (dt)=>{ dataRetorno += dt; });
				res.on('end', () =>{
					if(dataRetorno.length === 0)
					{
						_callback({'status':'error', 'message': 'Request no generó respuesta.', 'data' : null});
					}
					else
					{
						_callback({'status':'success', 'message': 'Request ejecutado sin problemas.','data':dataRetorno});
					}
				});
			}
		)
		.on('error', (e)=>{
			_callback({'status':'error', 'message': e.message, 'data' : null})
		});

		req.setTimeout( ((typeof _url.timeout !== 'undefined')? _url.timeout : 2000), function( ) {
		    req.abort();
		    _callback({'status':'error', 'message': 'Tiempo de respuesta elevado.', 'data' : null});
		});
	};

	this.post = function (_url,_headers,_param,isJsonData=false,_callback)
	{
		let options = {
			url: _url,
			method: 'POST',
			headers: _headers
		};
		if(isJsonData)
		{
			options.json = _param;
		}
		else
		{
			options.form = _param;
		}
		request(options, function(err, response, body){
			if(!err && response.statusCode=== 200)
			{
				_callback({'status':'success', 'message': 'Request ejecutado sin problemas.','data':body});
			}
			else
			{
				_callback({'status':'error', 'message': err, 'data' : body})
			}
		});

	};
};

module.exports = HttpRequest;