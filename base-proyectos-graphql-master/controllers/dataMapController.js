'use strict';
const u = require('./utilidadesController'),
reqHttp = require('./httpController');

const DataMap = new function() {
	this.respuesta = null;
	this.inicializarRespuesta = function(){
		this.respuesta = {
			'status':'success',
			'message': 'Datos obtenidos exitosamente',
			'data' : null
		};
	};

	this.generarRespuesta = function (_data){
		this.inicializarRespuesta();
		if(_data !== null && typeof _data !== 'undefined' && ( (typeof _data === 'object' && !Array.isArray(_data) && Object.keys(_data).length > 0 ) || (Array.isArray(_data) && _data.length > 0) ))
		{
			this.respuesta.data = _data;
		}
		else
		{
			this.respuesta.status = 'error';
			this.respuesta.message = 'Consulta no genero data';
		}
		return this.respuesta;
	};

};

let dmFolder = u.getFilesNamesInFolderFilter('./controllers/dataMap','.js');

dmFolder.forEach(r =>{
	let aux = require('./dataMap/'+r)(u,reqHttp);
	for (let key in aux) {
		DataMap[key]= aux[key];
	}
});

module.exports = DataMap;