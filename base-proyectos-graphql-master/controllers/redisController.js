'use strict';
const redis = require("redis");

const redisCon = new function(){
	this.redisConeccion = null;
	this.conexion = function(_host,_bd,_port)
	{
    let objCon = {
        db:_bd,
        retry_strategy: function (options) {
              if (options.error && options.error.code === 'ECONNREFUSED') {
                  return new Error('The server refused the connection');
              }
              if (options.total_retry_time > 1000 * 60 * 60) {
                  return new Error('Retry time exhausted');
              }
              if (options.attempt > 10) {
                  return undefined;
              }
              return Math.min(options.attempt * 100, 3000);
          }
    };
    if(process.env.redisAuth.length > 0){
      objCon.password = process.env.redisAuth;
    }
		this.redisConeccion = redis.createClient(
      _port,
      _host,
      objCon
      );
		return this.redisConeccion;
	}
};

module.exports = redisCon;