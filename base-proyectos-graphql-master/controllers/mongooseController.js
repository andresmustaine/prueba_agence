"use strict";

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const mongooseCon = new function(){
	this.mongoCon = {};
	this.conexion = function(_stringCon){
		this.mongoCon.con = mongoose.connect(_stringCon,{ useNewUrlParser: true });
		mongoose.set('useCreateIndex', true);
		this.mongoCon.mongo = mongoose;
		return this.mongoCon;
	}
};

module.exports = mongooseCon;