"use strict";
if (process.env.APP_ENV === 'dev'){
  require('dotenv').config({path:'./env-dev/.env'});
}
else {
  require('dotenv').config();
}
var createError = require('http-errors'),
express = require('express'),
path = require('path'),
cookieParser = require('cookie-parser'),
logger = require('morgan'),
helmet = require('helmet');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json({limit: '1gb', extended: true}));
app.use(express.urlencoded({limit: '1gb', extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,POST');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use('/', require('./routes/index'));

require('./routes/graphql').applyMiddleware({ app ,path: process.env.graphQlRuta});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
console.log('Aplicación "'+process.env.PROYECTO+'" ejecutandose bajo el ambiente de "'+process.env.APP_ENV+'", puerto: '+process.env.PORT);
module.exports = app;
