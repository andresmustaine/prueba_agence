## Proyecto Base con graphql

### Implementacion
1. Clona el proyecto
2. El proyecto tiene dos archivos .env, el de producción se encuentra en la ```base del proyecto``` y el de desarrollo en la carpeta ```"env-dev"```, las variables obligatorias son:

```
#Datos Base del Proyecto
PORT=
VERSION=0.1
PROYECTO=""
PAIS="CL"
formatoFecha="dd-mm-yyyy HH:MM:ss"

#Configuraciones GraphQl
graphQlRuta="/graphql"

#Conexiones de MySql
mysqlConHost=""
mysqlConUser=""
mysqlConPass=""
mysqlConDB1=""
mysqlConDB2=""
mysqlConDB3=""

#Conexiones de MongoDB (Cadena de conexión)
mongoCon=""

#Conexiones de Redis
redisConHost=""
redisPort=""
redisAuth=""
redisSelectDB=8
```
3. Debes instalar los plugins: (Leera los plugins dentro de ```package.json```)
```
npm install
```
4. Crear en gitlab un nuevo proyecto
5. Por ```ssh``` se debe cambiar el ```remote (referente a que origin apunta)``` al nuevo proyecto, en la base del mismo:
```
  ssh /proy# git remote -v
  ssh /proy# git remote set-url origin [url_nuevo_proyecto_gitlab.git]
  ssh /proy# git remote -v
```
----

### Levantar o ejecutar el proyecto
#### En Producción
```
env APP_ENV=production HOST=0 pm2 start npm --name "app_name" -- run start
```
#### En Desarrollo
```
env APP_ENV=dev HOST=0 pm2 start npm --name "app_name" -- run start --watch
```
----

### ¿Como es la estructura de graphql en el proyecto?

>>>
Mayormente se encuentra automatizada la carga de nuevos archivos.
¿Que quiere decir esto?
Cada vez que generes un nuevo resolver o un nuevo schema, se cargaran automaticamente una vez reiniciado el servicio
Estos archivos se encuentran en: *a) /graphql/resolvers y b) /graphql/schemas/*

Los resolvers deben seguir este patron *nombreResolver.js*, siendo obligatorio que termine en *Resolver.js* al igual que los schemas, estos siguen el patron *nombreSchema.js* con la imperancia de que termine en *Schema.js*, dado cuando se levanta el proceso, internamente buscara todos los archivos que tengan estos patrones ubicados en las carpetas *a) y b)*

Cuando se crea una nueva conexión a BD, se deberá al archivo *./graphql/graphModels.js* de forma manual de la siguiente forma:
  ```javascript
  'use strict';
  const newDB = require('../graphql/db/newDBCon');
  const newDB2 = require('../graphql/db/newDBCon2');
  module.exports = { newDB, newDB2};
  ```
>>>
#### ¿Para que sirve el dataMapController.js?
>>>
Es usado para procesar los datos y generar la estructura de la respuesta que se requiera retornar. Lee automaticamente los archivos en *./controllers/dataMap* y agrega las funciones al objeto dataMap declarado en los resolvers.
>>>
#### ¿Como personalizo mis archivos en *./controllers/dataMap*?
>>>
Es usado para procesar los datos y generar una respuesta
```javascript
'use strict';
module.exports = (u,reqHttp) => {
 this.functionProcessData = async function(_params){
  /// *código - do somethings *
 }
 return this;
};
```
*u: Funciones de utilidad, declaradas en el archivo: ./controllers/utilidadesController.js*

*reqHttp: Funciones para generar request post y get, declaradas en el archivo: ./controllers/httpController.js*
>>>
----
## Conexiones a Bases de datos
>>>
Las conexiones deberán ser creadas en la carpeta */graphql/db/* y deben llamarse de la siguiente forma ```"[tipoBD].[Base]Con.js".```

Internamente se manjeran por cada conexion, 3 subconexion: fast(solo para consultas simples), slow(Solo para updates) y complex(Para procesos más lentos)

Ejemplo: "Una base de datos de clienteA en MySql" => *mysqlClienteACon.js*
>>>
### MySql
>>>
Para trabajar MySql se usará el ORM Sequelize (http://docs.sequelizejs.com/)
>>>
##### ¿Como declaro la conexión Mysql?
>>>
Las conexión tendrán declaradas las tablas y las relaciones que tienen de ser necesario. 

Ejemplo:

  ```javascript
  const newDB = new function(){
    let mysqlCon = require('../../controllers/mysqlController');
    let con = mysqlCon.conexion(process.env.mysqlConHost,process.env.mysqlConDB, process.env.mysqlConUser, process.env.mysqlConPass);
    this.sq = con.sq;
    this.tabla1 = require('./mysql/valecart.tabla1Model')(con, con.sq);

    return this;
  }
  const newDBExport = {fast: newDB, slow: newDB, complex:newDB};
  module.exports = newDBExport;
  ```
  ```La nomenclatura de las tablas es la siguiente [BD].[tabla]Model.js```

  La tabla en este ejemplo, fisicamente se debe crear en la siguiente ruta *./graphql/db/mysql/valecart.tabla1Model.js* y contendra la estructura declarada en MySql:
  ```javascript
  'use strict';
  module.exports = (db, dt) =>{
    return db.con.define('nombre_tabla',
    {
      field1 :{
        type: dt.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      field2 :{
        type: dt.STRING,
        allowNull : false
      },
      field3 :{
        type: dt.INTEGER,
        allowNull : true
      },
      fechaCreacion: { type: dt.STRING }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true,
      underscored: true
    });
  }
  ```
  **** recomiendo usar el tipo de dato String para las fechas dado que es mas fácil de manejar.
>>>
##### ¿Cuales son los tipos de datos de Sequelize?
```javascript
  Sequelize.STRING                      // VARCHAR(255)
	Sequelize.STRING(1234)                // VARCHAR(1234)
	Sequelize.STRING.BINARY               // VARCHAR BINARY
	Sequelize.TEXT                        // TEXT
	Sequelize.TEXT('tiny')                // TINYTEXT
	Sequelize.INTEGER                     // INTEGER
	Sequelize.BIGINT                      // BIGINT
	Sequelize.BIGINT(11)                  // BIGINT(11)
	Sequelize.FLOAT                       // FLOAT
	Sequelize.FLOAT(11)                   // FLOAT(11)
	Sequelize.FLOAT(11, 12)               // FLOAT(11,12)
	Sequelize.REAL                        // REAL        PostgreSQL only.
	Sequelize.REAL(11)                    // REAL(11)    PostgreSQL only.
	Sequelize.REAL(11, 12)                // REAL(11,12) PostgreSQL only.
	Sequelize.DOUBLE                      // DOUBLE
	Sequelize.DOUBLE(11)                  // DOUBLE(11)
	Sequelize.DOUBLE(11, 12)              // DOUBLE(11,12)
	Sequelize.DECIMAL                     // DECIMAL
	Sequelize.DECIMAL(10, 2)              // DECIMAL(10,2)
	Sequelize.DATE                        // DATETIME for mysql / sqlite, TIMESTAMP WITH TIME ZONE for postgres
	Sequelize.DATE(6)                     // DATETIME(6) for mysql 5.6.4+. Fractional seconds support with up to 6 digits of precision
	Sequelize.DATEONLY                    // DATE without time.
	Sequelize.BOOLEAN                     // TINYINT(1)
	Sequelize.ENUM('value 1', 'value 2')  // An ENUM with allowed values 'value 1' and 'value 2'
	Sequelize.ARRAY(Sequelize.TEXT)       // Defines an array. PostgreSQL only.
	Sequelize.ARRAY(Sequelize.ENUM)       // Defines an array of ENUM. PostgreSQL only.
	Sequelize.JSON                        // JSON column. PostgreSQL, SQLite and MySQL only.
	Sequelize.UUID                        // UUID datatype for PostgreSQL and SQLite, CHAR(36) BINARY for MySQL (use defaultValue: Sequelize.UUIDV1 or Sequelize.UUIDV4 to make sequelize generate the ids automatically)
	Sequelize.GEOMETRY                    // Spatial column.  PostgreSQL (with PostGIS) or MySQL only.
	Sequelize.GEOMETRY('POINT')           // Spatial column with geometry type. PostgreSQL (with PostGIS) or MySQL only.
	Sequelize.GEOMETRY('POINT', 4326)     // Spatial column with geometry type and SRID.  PostgreSQL (with PostGIS) or MySQL only
```
##### ¿Como realizo una consulta?
>>>
Para realizar una consulta, asumiendo el uso de un resolver:
```javascript
  let data = await newDB.fast.tabla1.find({where:{field1:args.param}});
```
>>>
----
### MongoDB
>>>
Para trabajar MongoDB se usará el plugin Mongoose, las configuraciones de conexion pueden variar segun la versión de MongoDB, en caso de requerir modificarla se debe realizar en el archivo *./controllers/mongooseController.js*
>>>
##### ¿Como declaro la conexión MongoDB?
>>>
Las conexión tendrán declaradas los schemas. 

Ejemplo:

  ```javascript
  "use strict";
const newDB2 = new function(){
	const mongo = require('../../controllers/mongooseController');
	let con = mongo.conexion(process.env.mongoCon);
	this.schema1 = require('./mongodb/bd.schemaModel')(con,"nameSchema");

	return this;
}

const newDB2Export = {fast: newDB2,slow: newDB2,complex: newDB2};

module.exports = newDB2Export;
```
  ```La nomenclatura de los schemas es la siguiente [BD].[schema]Model.js```

  El schema en este ejemplo, fisicamente se debe crear en la siguiente ruta *./graphql/db/mongodb/bd.schemaModel.js* y contendra la estructura declarada en MongoDB:
  ```javascript
 'use strict';

module.exports = (db,schema) => {
	return db.mongo.model(schema, new db.mongo.Schema({
		field1: String,
		field2: String,
		field3: String,
		field4: Number
	},{collection: schema}));
}
```
>>>

##### ¿Cuales son los tipos de datos de Sequelize?
>>>

Los tipos de datos son los siguientes:

```
Mongo - mongoose:
	String
	Number
	Date
	Buffer
	Boolean
	Mixed
	Objectid
	Array
```
>>>

##### ¿Como realizo una consulta?
>>>
Para realizar una consulta, asumiendo el uso de un resolver:
```javascript
  let data = await newDB2.fast.shema1.find();
```
>>>
----

### Redis
##### ¿Como declaro la conexión Redis?
>>>
Las conexión tendrán declaradas los keys. 
Ejemplo:
```javascript
'use strict';

const redis = new function(){
	const redisCon = require('../../controllers/redisController');
	let con = redisCon.conexion(process.env.redisConHost,process.env.redisSelectDB,process.env.redisPort);

	this.keyData = require('./redis/baseModel')(con,"key.subKey:id");
}

const redisExport = {fast:redis, slow:redis, complex:redis};
module.exports = redisExport;
```
La nomenclatura de los keys es la siguiente ```[key].[subKey].[subsubKey]:[identificador]```

El modelo, fisicamente se encuentra en la siguiente ruta *./graphql/db/redis/baseModel.js*, este archivo es la definición estructural recomendada para todas las keys, dado que es generico y posee las siguientes funciones: *find(), findAll(), set(), clear().*
>>>
##### ¿Como realizo una consulta?
>>>
Para realizar una consulta, asumiendo el uso de un resolver:
```javascript
  let data = await dbRedis.fast.keyData.find(idToSearch);
```
>>>